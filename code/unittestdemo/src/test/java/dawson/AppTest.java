package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

    @Test
    public void echoReturnX()
    {
        assertEquals("Check if echo returns x value", 5, App.echo(5));
    }

    @Test
    public void oneMoreReturn()
    {
        assertEquals("Check if oneMore returns x value + 1", 6, App.oneMore(5));
    }
}
